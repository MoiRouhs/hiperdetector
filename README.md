# hiperdetector

Una herramienta para aprender a programar desde la detección de movimiento, esta une varías librerías como p5js, vida, una recopilación de código propio y otros que hace más fácil el enamorarse de la programación creativa.

## Requisitos para correr

1. Tener instalado node.js
2. Tener instalado npm
3. Dentro de la carpeta hiperdetector correr:``

~~~
npm install
~~~

## Iniciar hiperdetector

~~~
npm start
~~~

